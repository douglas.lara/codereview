package io.doug.codereview.config;

import org.gitlab4j.api.GitLabApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public GitLabApi getGitLabApi(@Value("${gitlab.server-url}") final String serverUrl, @Value("${gitlab.access-token}") String accessToken) {
     return new GitLabApi(serverUrl, accessToken);
    }
}
