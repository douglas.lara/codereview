package io.doug.codereview.servers.gitlab;

import lombok.AllArgsConstructor;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class GitlabService {

    private GitLabApi gitLabApi;

    public List<String> getProjectNames() throws GitLabApiException {

        List<Project> projectList = gitLabApi
                .getProjectApi().getOwnedProjects();
        return projectList.stream()
                .map(Project::getName)
                .collect(Collectors.toList());
    }

}
