package io.doug.codereview.servers.gitlab;

import org.gitlab4j.api.GitLabApiException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GitlabServiceTest {

    @Autowired
    GitlabService gitlabService;

    @Test
    public void listProjectsTest() throws GitLabApiException {
        List<String> projects = gitlabService.getProjectNames();
        projects.forEach(System.out::println);
    }

}